-- Verify sqitch_sandbox:users on snowflake

USE WAREHOUSE &warehouse;

SELECT nickname, password, fullname, twitter, timestamp
  FROM unwrapt.users
WHERE FALSE;
