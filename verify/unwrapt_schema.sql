-- Verify unwrapt:unwraptSchema on snowflake

USE WAREHOUSE &warehouse;

SELECT 1/COUNT(*) FROM information_schema.schemata WHERE schema_name = 'UNWRAPT';