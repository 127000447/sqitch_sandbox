-- Deploy sqitch_sandbox:users to snowflake
-- requires: unwrapt_schema

USE WAREHOUSE &warehouse;

CREATE TABLE unwrapt.users (
    nickname  TEXT         PRIMARY KEY,
    password  TEXT         NOT NULL,
    fullname  TEXT         NOT NULL,
    twitter   TEXT         NOT NULL,
    timestamp TIMESTAMP_TZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);
